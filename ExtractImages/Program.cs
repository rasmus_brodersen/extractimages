﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Dapper;
using System.Drawing;

namespace ExtractImages {
    class Program {
        static void Main(string[] args) {

            //SqlConnection connection = new SqlConnection(@"Data Source=DKCPW00365;Initial Catalog=Frederikshavn_GIS;Integrated Security=SSPI;Persist Security Info=False;");
            SqlConnection connection = new SqlConnection(@"Data Source=mssql2014.mssql.gdev.local\sql2014pmc2;Initial Catalog=RoSyAle_CS;Persist Security Info=True;User ID=rbr;Password=rbr");
            
            
            connection.Open();

            List<FeatObj> feats = connection.Query<FeatObj>("Select FOName, Picture from biroFeatureObjects where Picture IS NOT NULL").ToList();

            var exportPath = @"C:\_myProjects\_TrafficSigns_Lyr\sv_nye_skilte";
            if (!Directory.Exists(exportPath)) {
                Directory.CreateDirectory(exportPath);
            }
            foreach (FeatObj row in feats) {
                var imageBytes = row.Picture;
                if (imageBytes.Length > 0) {
                    using (var convertedImage = new System.Drawing.Bitmap(new MemoryStream(imageBytes))) {
                        int BlankOrLast = row.FOName.IndexOf(" ");
                        if (BlankOrLast == -1) BlankOrLast = row.FOName.Length; 
                        string fname = row.FOName.Substring(0, BlankOrLast);
                        fname = fname.Replace("/","_");
                        var fileName = Path.Combine(exportPath, fname + ".bmp"); //fname
                        if (File.Exists(fileName)) {
                            File.Delete(fileName);
                        }
                        convertedImage.Save(fileName);
                    }
                }
            }
        }
    }
}