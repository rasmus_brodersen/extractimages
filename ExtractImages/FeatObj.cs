﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractImages {
    class FeatObj {
        public string FOName { get; set; }
        public Byte[] Picture { get; set; }
    }
}
